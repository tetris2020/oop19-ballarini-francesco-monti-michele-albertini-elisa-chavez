package gamegraphics;

/**
 * Interface for the Logics of the BoxPanel.
 *
 */
public interface BoxPanelLogics extends GridPanelLogics {
    /**
     * Lowers the dimensions (height and width) of the BoxPanel.
     * 
     */
    void lowerDimensions();
}
