package login;

/**
 * Enumeration that is used during the login part of the application.
 */
public enum LoginTypes {
    /**
     * Enum that identifies the view for a new player.
     */
    NEW,
    /**
     * Enum that identifies the view for an existing user.
     */
    OLD,
    /**
     * Enum that identifies the general view of the login part.
     */
    GENERAL
}
